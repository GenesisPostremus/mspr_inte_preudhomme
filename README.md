#Installation des requierements

````
pip install -r requirements.txt
````

#Démarrer le projet

````
env/bin/python3.8 mspr_inte_preudhomme/manage.py runserver
````

#Pour lancer les tests

````
pytest --ds=mspr_inte_preudhomme.settings --junitxml=reports/junit.xml --pylint --pylint-output-file=reports/pylint.report --cov-report=xml:reports/coverage.xml --cov=erp
````

#Sinon ça c'est le trello

https://trello.com/b/cn3TsZku/mise-en-%C5%93uvre-de-lintegration-continue

# Encart
